-Dproduct.version=${product.version}
-Dproduct.data.version=${product.data.version}
-Dhttp.port.bak=${it.http.port}
-Drmi.port.bak=${it.rmi.port}
${invoker.product}:run org.codehaus.gmaven:gmaven-plugin::execute ${invoker.product}:cli org.codehaus.gmaven:gmaven-plugin::execute -Dallow.google.tracking=false -DskipAllPrompts=true ${invoker.product}:stop
clean
